#include <Arduino.h>
#include <WiFi.h>
#include <Wire.h>
#include "PubSubClient.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_BME680.h"
#include "ArduinoJson.h"
#include "configuration.h"

void initializeWiFi();
void initializeSensor();
void initializeMqtt();
boolean connectMqtt();
void publishMessage();
void callback(char* topic, byte* payload, unsigned int length);

struct dataPoint
{
  float temperature = 0.0F;
  float humidity = 0.0F;
};

WiFiClient net;
PubSubClient mqttClient(net);

const char ssid[] = SECRET_SSID;
const char password[] = SECRET_PASS;
const char broker[] = SECRET_BROKER;
String measurementTopic = PUBLISH_TOPIC;;
String actionTopic = SUBSCRIBE_TOPIC;

Adafruit_BME680 bme680;
unsigned long lastMillis = 0;
long lastReconnectAttempt = 0;

void initializeSensor()
{
   if (!bme680.begin())
   {
     Serial.println("Could not find a valid BME680!");
     while(1);
   }
   bme680.setTemperatureOversampling(BME680_OS_8X);
   bme680.setHumidityOversampling(BME680_OS_2X);
}

void initializeWiFi()
{
  delay(10);
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting WiFi to: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (true)
  {
    Serial.print(".");
    if (WiFi.status() == WL_CONNECTED)
    {
      Serial.println("WiFi Connected");
      Serial.print("IP Address: ");
      Serial.println(WiFi.localIP());
      Serial.print("RSSI: ");
      Serial.println(WiFi.RSSI());
      break;
    }
  }
}

void initializeMqtt()
{
  mqttClient.setServer(broker, PORT);
  mqttClient.setCallback(callback);
  lastReconnectAttempt = 0;
}

boolean connectMqtt()
{
  Serial.println("Connecting MQTT .....");

  if (mqttClient.connect(CLIENT_ID, MQTT_USERNAME, MQTT_PASSWORD))
  {
    Serial.println("Connected to MQTT broker");
    mqttClient.subscribe(actionTopic.c_str(), 1);
  }

  else
  {
    Serial.print("Failed connecting to broker. Error state= ");
    Serial.println(mqttClient.state());
  }
  
  return mqttClient.connected();
}

void publishMessage()
{
  if (!bme680.performReading())
  {
    Serial.println("Failed to perform reading");
    return;
  }

  unsigned long endTime = bme680.beginReading();

  if (endTime == 0)
  {
    Serial.println("Failed to begin reading.");
    return;
  }

  if (!bme680.endReading())
  {
    Serial.println("Failed to complete reading.");
    return;
  }

  static dataPoint data;
  static char payload[256];
  StaticJsonDocument<256>doc;
  
  float t = bme680.temperature;
  float h = bme680.humidity;

  data.temperature = t;
  data.humidity = h;

  doc["temperature"] = data.temperature;
  doc["humidity"] = data.humidity;

  serializeJsonPretty(doc, payload);
  mqttClient.publish(measurementTopic.c_str(), payload);
  Serial.println(payload);
}

void callback(char* topic, byte* payload, unsigned int length)
{
  String callTopic = "";
  int i = 0;

  while ((char)topic[i] != 0)
  {
    callTopic += (char)topic[i];
    i++;
  }

  if (callTopic.equals(actionTopic.c_str()))
  {
    if ((char)payload[0] == '1')
    {
      digitalWrite(19, HIGH);
      Serial.println("LED ON");
    }

    else if((char)payload[0] == '0')
    {
      digitalWrite(19, LOW);
      Serial.println("LED OFF");
    }
    
  }
  
}

void setup() {

  Serial.begin(9600);
  pinMode(19, OUTPUT);
  initializeSensor();
  initializeWiFi();
  initializeMqtt();

}

void loop() {

  if (WiFi.status() != WL_CONNECTED)
  {
    ESP.restart();
    initializeWiFi();
  }

  if (WiFi.status() == WL_CONNECTED && !mqttClient.connected())
  {
    long now = millis();

    if (now - lastReconnectAttempt > 2000)
    {
      lastReconnectAttempt = now;

      if (connectMqtt())
      {
        lastReconnectAttempt = 0;
      }
      
    }
    
  }

  else
  {
    mqttClient.loop();
  }
  
  if (millis() - lastMillis > 30000)
  {
    lastMillis = millis();
    publishMessage();
  }
  
}